var gulp = require("gulp"),  
watch = require("gulp-watch"),
browserSync = require("browser-sync").create()
gulp.task("watch", function(){
    browserSync.init({
       notify: true,
        server:{
            baseDir: "app"
        }
    });
//    watch("./app/index.html",gulp.series("html")); //without reload
    watch("./app/index.html",function(){
        browserSync.reload();
    });//with reload
    watch("./app/assets/styles/**/*.css", gulp.series('css' , 'cssInject'));//this will only automatically sync the html file only so for css sync the cssInject function is used
});

gulp.task("cssInject", function(){
   return gulp.src("./app/css/styles.css")
    .pipe(browserSync.stream());
});